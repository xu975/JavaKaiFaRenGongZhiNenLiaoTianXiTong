<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
-->
<style type="text/css">
	* {
		margin: 0;
		padding: 0;
	}
	html,body {
		width: 100%;
		height: 100%;
	}
	body {
		background: url(img/bg.jpg) no-repeat center;
	}
	/* title start */
	.title {
		width: 100%;
		height: 60px;
		background: rgba(0,0,0,0.2);
		font-size: 28px;
		font-weight: bolder;
		color: white;
		text-align: center;
		line-height: 60px;
	}
	/* title end */
	
	/* center start */
	.center {
		width: 800px;
		height: 600px;
		border: 1px solid red;
		margin: 100px auto 0;
		background: rgba(255,255,255,.3);
	}
	.center .c_header {
		width: 100%;
		height: 60px;
		background: #2196F3;
	}
	.center .c_center {
		width: 780px;
		height: 440px;
		padding: 10px;
		overflow: auto;
	}
	.center .c_center .rot {
		width: 100%;
		overflow: hidden;
	}
	.center .c_center .rot span {
		width: 40px;
		height: 40px;
		background: url(img/rot.png);
		display: block;
		float: left;
	}
	.center .c_center .rot p {
		max-width: 220px;
		float: left;
		padding: 10px;
		background: rgba(0,0,255,.5);
		border-radius: 8px;
		margin-left: 10px;	
		color: white;
	}
	.center .c_center .my {
		width: 100%;
		overflow: hidden;
	}
	.center .c_center .my span {
		width: 40px;
		height: 40px;
		background: url(img/my.png);
		display: block;
		float: right;
	}
	.center .c_center .my p {
		float: right;
		padding: 10px;
		background: #19b955;
		border-radius: 8px;
		margin-right: 10px;
		max-width: 220px;
		color: white;
	}
	.center .c_footer {
		width: 100%;
		height: 80px;
	}
	#text {
		width: 640px;
		height: 60px;
		outline: none;
		text-indent: 20px;
		font-size: 18px;
		background: rgba(0,0,0,.4);
		border: none;
		color: white;
		border-radius: 10px 10px;
		margin-left: 25px;
	}
	#btn {
		width: 100px;
		height: 60px;
		outline: none;
		font-size: 18px;
		background: rgba(0,0,0,.4);
		border: none;
		color: white;
		border-radius: 10px 10px;
	}
	/* center end */
</style>
</head>

<body>
	<!-- title start -->
	<div class="title">
		Java开发人工智能机器人
	</div>
	<!-- title end -->
	
	<!-- center start -->
	<div class="center">
		<div class="c_header"></div>
		<div class="c_center">
			<div class="rot">
				<span></span>
				<p>你好！</p>
			</div>
		</div>
		<div class="c_footer">
			<input type="text" id="text">
			<input type="button" id="btn" value="发送">
		</div>
	</div>
	<!-- center end -->
	
	<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript">
		$("#btn").click(function(){
			var text = $("#text").val();
			if (text == "" || text == null) {
				return;
			} else {
				var my = 
					"<div class='my'>"
						+"<span></span>"
						+"<p>"+text+"</p>"
					+"</div>";
				$(".c_center").append(my);
				$("#text").val("");
				$("#text").focus();
				$(".c_center").scrollTop(999999999999);
				$.ajax({
					url: "chart",// 提交的地址ChartActionServlet
					type: "post",// 提交的方式
					data: {"text":text},// 传递的参数
					success: function(reslut){// 成功的事件
						var json = $.parseJSON(reslut);
						var obj = 
							"<div class='rot'>"
								+"<span></span>"
								+"<p>"+json.text+"</p>"
							+"</div>";
						$(".c_center").append(obj);
						$(".c_center").scrollTop(999999999999);
					}
				});
			}		
		});
		$(document).keydown(function(event){
			if (event.keyCode == 13) {
				var obtnLogin = document.getElementById("btn");
				obtnLogin.focus();
			}
		});
	</script>
</body>
</html>
