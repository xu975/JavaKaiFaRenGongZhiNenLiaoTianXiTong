package com.jbit.controller;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 聊天请求类
 * @author 许前程
 *
 */
@WebServlet("/chart")
public class ChartAction extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String text = request.getParameter("text");
		// 图灵机器人的APIKEY
		String APIKEY = "698df73c585a4132ad6e1cfb191cc075";
		// 请求的内容，编码方式为UTF-8
		String info = URLEncoder.encode(text, "utf-8");
		// 用户的唯一标志（对应自己的每一个用户）
		String userId = "144779";
		// 图灵机器人的请求路径
		String getURL = "http://www.tuling123.com/openapi/api?key=" + APIKEY
				+ "&info=" + info + "&userid=" + userId;
		// 创建url资源
		URL getUrl = new URL(getURL);
		// 建立http连接
		HttpURLConnection conn = (HttpURLConnection) getUrl
				.openConnection();
		// 设置传递方式
        conn.setRequestMethod("POST");
        // 设置维持长连接
        conn.setRequestProperty("Connection", "Keep-Alive");
        // 设置文件字符集
        conn.setRequestProperty("Charset", "UTF-8");
		// 开始连接请求
		conn.connect();
		// 取得输入流，并使用Reader读取
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				conn.getInputStream(), "utf-8"));
		StringBuffer sb = new StringBuffer();
		String line = "";
		// 循环判断是否为空
		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}
		// 关闭读取流
		reader.close();
		// 关闭连接
		conn.disconnect();
		// 把返回的结果输出到页面上
		response.getWriter().print(sb);
	}
	
	
}
